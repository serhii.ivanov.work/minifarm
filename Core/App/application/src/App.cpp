#include <iostream>
#include <Core/App/application/include/App.hpp>
#include <SFML/Graphics.hpp>

App::App()
{
	std::cout << "Hello I am App." << std::endl;
    green_circle();
}

void App::green_circle()
{
    sf::RenderWindow window(sf::VideoMode({ 200, 200 }), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        window.draw(shape);
        window.display();
    }
}
